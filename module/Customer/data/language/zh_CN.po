msgid ""
msgstr ""
"Project-Id-Version: DBErp\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-26 18:02+0800\n"
"PO-Revision-Date: 2019-04-26 18:02+0800\n"
"Last-Translator: Baron <baron@loongdom.cn>\n"
"Language-Team: DBErp <baron@loongdom.cn>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: translate\n"
"X-Poedit-Basepath: .\n"
"X-Generator: Poedit 2.0.7\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: ../..\n"

#: ../../src/Controller/CustomerCategoryController.php:70
#, php-format
msgid "客户分类 %s 添加成功！"
msgstr ""

#: ../../src/Controller/CustomerCategoryController.php:71
#: ../../src/Controller/CustomerCategoryController.php:106
#: ../../src/Controller/CustomerCategoryController.php:128
#: ../../src/Controller/CustomerCategoryController.php:154
#: ../../view/customer/customer/add.phtml:79
#: ../../view/customer/customer/index.phtml:28
msgid "客户分类"
msgstr ""

#: ../../src/Controller/CustomerCategoryController.php:91
#: ../../src/Controller/CustomerCategoryController.php:147
msgid "该客户分类不存在！"
msgstr ""

#: ../../src/Controller/CustomerCategoryController.php:105
#, php-format
msgid "客户分类 %s 编辑成功！"
msgstr ""

#: ../../src/Controller/CustomerCategoryController.php:127
#: ../../src/Controller/CustomerController.php:145
#: ../../src/Controller/SupplierCategoryController.php:127
#: ../../src/Controller/SupplierController.php:148
msgid "批量处理成功！"
msgstr ""

#: ../../src/Controller/CustomerCategoryController.php:153
#, php-format
msgid "客户分类 %s 删除成功！"
msgstr ""

#: ../../src/Controller/CustomerCategoryController.php:157
#, php-format
msgid "客户分类 %s 删除失败！其下还有客户信息"
msgstr ""

#: ../../src/Controller/CustomerController.php:87
#, php-format
msgid "客户 %s 添加成功！"
msgstr ""

#: ../../src/Controller/CustomerController.php:88
#: ../../src/Controller/CustomerController.php:124
#: ../../src/Controller/CustomerController.php:146
#: ../../src/Controller/CustomerController.php:172
msgid "客户"
msgstr ""

#: ../../src/Controller/CustomerController.php:108
#: ../../src/Controller/CustomerController.php:165
msgid "该客户不存在！"
msgstr ""

#: ../../src/Controller/CustomerController.php:123
#, php-format
msgid "客户 %s 编辑成功！"
msgstr ""

#: ../../src/Controller/CustomerController.php:171
#, php-format
msgid "客户 %s 删除成功！"
msgstr ""

#: ../../src/Controller/SupplierCategoryController.php:70
#, php-format
msgid "供应商分类 %s 添加成功！"
msgstr ""

#: ../../src/Controller/SupplierCategoryController.php:71
#: ../../src/Controller/SupplierCategoryController.php:106
#: ../../src/Controller/SupplierCategoryController.php:128
#: ../../src/Controller/SupplierCategoryController.php:154
#: ../../view/customer/supplier/add.phtml:79
#: ../../view/customer/supplier/index.phtml:30
msgid "供应商分类"
msgstr ""

#: ../../src/Controller/SupplierCategoryController.php:91
#: ../../src/Controller/SupplierCategoryController.php:147
msgid "该供应商分类不存在！"
msgstr ""

#: ../../src/Controller/SupplierCategoryController.php:105
#, php-format
msgid "供应商分类 %s 编辑成功！"
msgstr ""

#: ../../src/Controller/SupplierCategoryController.php:153
#, php-format
msgid "供应商分类 %s 删除成功！"
msgstr ""

#: ../../src/Controller/SupplierCategoryController.php:157
#, php-format
msgid "供应商分类 %s 删除失败！其下还有供应商信息"
msgstr ""

#: ../../src/Controller/SupplierController.php:90
#, php-format
msgid "供应商 %s 添加成功！"
msgstr ""

#: ../../src/Controller/SupplierController.php:91
#: ../../src/Controller/SupplierController.php:127
#: ../../src/Controller/SupplierController.php:149
#: ../../src/Controller/SupplierController.php:175
msgid "供应商"
msgstr ""

#: ../../src/Controller/SupplierController.php:111
#: ../../src/Controller/SupplierController.php:168
msgid "该供应商不存在！"
msgstr ""

#: ../../src/Controller/SupplierController.php:126
#, php-format
msgid "供应商 %s 编辑成功！"
msgstr ""

#: ../../src/Controller/SupplierController.php:174
#, php-format
msgid "供应商 %s 删除成功！"
msgstr ""

#: ../../src/Form/SearchCustomerForm.php:45
#: ../../src/Form/SearchSupplierForm.php:45
msgid "起始ID"
msgstr ""

#: ../../src/Form/SearchCustomerForm.php:55
#: ../../src/Form/SearchSupplierForm.php:55
msgid "结束ID"
msgstr ""

#: ../../src/Form/SearchCustomerForm.php:65
#: ../../view/customer/customer/add.phtml:11
#: ../../view/customer/customer/add.phtml:87
#: ../../view/customer/customer/index.phtml:26
msgid "客户名称"
msgstr ""

#: ../../src/Form/SearchCustomerForm.php:75
#: ../../view/customer/customer/add.phtml:7
#: ../../view/customer/customer/add.phtml:96
#: ../../view/customer/customer/index.phtml:27
msgid "客户编码"
msgstr ""

#: ../../src/Form/SearchSupplierForm.php:65
#: ../../view/customer/supplier/add.phtml:11
#: ../../view/customer/supplier/add.phtml:87
#: ../../view/customer/supplier/index.phtml:26
msgid "供应商名称"
msgstr ""

#: ../../src/Form/SearchSupplierForm.php:75
#: ../../view/customer/supplier/add.phtml:7
#: ../../view/customer/supplier/add.phtml:96
#: ../../view/customer/supplier/index.phtml:27
msgid "供应商编码"
msgstr ""

#: ../../src/Form/SearchSupplierForm.php:85
#: ../../view/customer/supplier/index.phtml:28
msgid "供应商联系人"
msgstr ""

#: ../../src/Form/SearchSupplierForm.php:95
msgid "供应商电话"
msgstr ""

#: ../../src/Plugin/CustomerCommonPlugin.php:46
msgid "选择供应商分类"
msgstr ""

#: ../../src/Plugin/CustomerCommonPlugin.php:63
msgid "选择供应商"
msgstr ""

#: ../../src/Plugin/CustomerCommonPlugin.php:80
msgid "选择客户分类"
msgstr ""

#: ../../src/Plugin/CustomerCommonPlugin.php:97
msgid "选择客户"
msgstr ""

#: ../../view/customer/customer-category/add.phtml:7
#: ../../view/customer/customer-category/add.phtml:42
msgid "客户分类编码"
msgstr ""

#: ../../view/customer/customer-category/add.phtml:11
#: ../../view/customer/customer-category/add.phtml:50
msgid "客户分类名称"
msgstr ""

#: ../../view/customer/customer-category/add.phtml:15
msgid "客户分类排序"
msgstr ""

#: ../../view/customer/customer-category/add.phtml:24
msgid "返回客户分类列表"
msgstr ""

#: ../../view/customer/customer-category/add.phtml:25
#: ../../view/customer/customer-category/add.phtml:70
msgid "保存客户分类"
msgstr ""

#: ../../view/customer/customer-category/add.phtml:36
msgid "编辑客户分类"
msgstr ""

#: ../../view/customer/customer-category/add.phtml:36
#: ../../view/customer/customer-category/index.phtml:7
msgid "添加客户分类"
msgstr ""

#: ../../view/customer/customer-category/add.phtml:58
#: ../../view/customer/customer-category/index.phtml:29
#: ../../view/customer/customer/add.phtml:200
#: ../../view/customer/customer/index.phtml:29
#: ../../view/customer/supplier-category/add.phtml:58
#: ../../view/customer/supplier-category/index.phtml:29
#: ../../view/customer/supplier/add.phtml:200
#: ../../view/customer/supplier/index.phtml:31
msgid "排序"
msgstr ""

#: ../../view/customer/customer-category/add.phtml:100
msgid "客户分类编码不能为空！"
msgstr ""

#: ../../view/customer/customer-category/add.phtml:103
msgid "客户分类名称不能为空！"
msgstr ""

#: ../../view/customer/customer-category/add.phtml:106
#: ../../view/customer/supplier-category/add.phtml:106
msgid "排序不能为空！"
msgstr ""

#: ../../view/customer/customer-category/add.phtml:107
#: ../../view/customer/supplier-category/add.phtml:107
msgid "排序必须为数字！"
msgstr ""

#: ../../view/customer/customer-category/add.phtml:108
#: ../../view/customer/supplier-category/add.phtml:108
msgid "排序最小为1！"
msgstr ""

#: ../../view/customer/customer-category/index.phtml:27
#: ../../view/customer/supplier-category/index.phtml:27
msgid "分类编码"
msgstr ""

#: ../../view/customer/customer-category/index.phtml:28
#: ../../view/customer/supplier-category/index.phtml:28
msgid "分类名称"
msgstr ""

#: ../../view/customer/customer-category/index.phtml:30
#: ../../view/customer/customer/index.phtml:30
#: ../../view/customer/supplier-category/index.phtml:30
#: ../../view/customer/supplier/index.phtml:32
msgid "操作"
msgstr ""

#: ../../view/customer/customer-category/index.phtml:44
#: ../../view/customer/customer/index.phtml:84
#: ../../view/customer/supplier-category/index.phtml:44
#: ../../view/customer/supplier/index.phtml:97
msgid "编辑"
msgstr ""

#: ../../view/customer/customer-category/index.phtml:47
msgid "您确实要删除该客户分类吗？"
msgstr ""

#: ../../view/customer/customer-category/index.phtml:48
#: ../../view/customer/customer/index.phtml:88
#: ../../view/customer/supplier-category/index.phtml:48
#: ../../view/customer/supplier/index.phtml:101
msgid "删除"
msgstr ""

#: ../../view/customer/customer-category/index.phtml:57
#: ../../view/customer/customer/index.phtml:97
#: ../../view/customer/supplier-category/index.phtml:57
#: ../../view/customer/supplier/index.phtml:111
msgid "全选"
msgstr ""

#: ../../view/customer/customer-category/index.phtml:59
#: ../../view/customer/customer/index.phtml:99
#: ../../view/customer/supplier-category/index.phtml:59
#: ../../view/customer/supplier/index.phtml:113
msgid "选择状态"
msgstr ""

#: ../../view/customer/customer-category/index.phtml:60
#: ../../view/customer/customer/index.phtml:100
#: ../../view/customer/supplier-category/index.phtml:60
#: ../../view/customer/supplier/index.phtml:114
msgid "更新排序"
msgstr ""

#: ../../view/customer/customer-category/index.phtml:62
#: ../../view/customer/customer/index.phtml:102
#: ../../view/customer/supplier-category/index.phtml:62
#: ../../view/customer/supplier/index.phtml:116
msgid "更新"
msgstr ""

#: ../../view/customer/customer/add.phtml:15
msgid "客户排序"
msgstr ""

#: ../../view/customer/customer/add.phtml:19
msgid "地址"
msgstr ""

#: ../../view/customer/customer/add.phtml:23
#: ../../view/customer/customer/add.phtml:131
#: ../../view/customer/supplier/add.phtml:23
#: ../../view/customer/supplier/add.phtml:131
msgid "电子邮箱"
msgstr ""

#: ../../view/customer/customer/add.phtml:27
#: ../../view/customer/customer/add.phtml:107
#: ../../view/customer/supplier/add.phtml:27
#: ../../view/customer/supplier/add.phtml:107
msgid "联系人"
msgstr ""

#: ../../view/customer/customer/add.phtml:31
#: ../../view/customer/supplier/add.phtml:31
msgid "手机"
msgstr ""

#: ../../view/customer/customer/add.phtml:35
#: ../../view/customer/supplier/add.phtml:35
msgid "座机"
msgstr ""

#: ../../view/customer/customer/add.phtml:39
#: ../../view/customer/customer/add.phtml:176
#: ../../view/customer/supplier/add.phtml:39
#: ../../view/customer/supplier/add.phtml:176
msgid "开户银行"
msgstr ""

#: ../../view/customer/customer/add.phtml:43
#: ../../view/customer/supplier/add.phtml:43
msgid "开户行账号"
msgstr ""

#: ../../view/customer/customer/add.phtml:47
#: ../../view/customer/customer/add.phtml:190
#: ../../view/customer/supplier/add.phtml:47
#: ../../view/customer/supplier/add.phtml:190
msgid "税号"
msgstr ""

#: ../../view/customer/customer/add.phtml:51
#: ../../view/customer/customer/add.phtml:207
#: ../../view/customer/supplier/add.phtml:51
#: ../../view/customer/supplier/add.phtml:207
msgid "备注"
msgstr ""

#: ../../view/customer/customer/add.phtml:59
msgid "返回客户列表"
msgstr ""

#: ../../view/customer/customer/add.phtml:60
#: ../../view/customer/customer/add.phtml:217
msgid "保存客户"
msgstr ""

#: ../../view/customer/customer/add.phtml:71
msgid "编辑客户"
msgstr ""

#: ../../view/customer/customer/add.phtml:71
#: ../../view/customer/customer/index.phtml:7
msgid "添加客户"
msgstr ""

#: ../../view/customer/customer/add.phtml:114
#: ../../view/customer/supplier/add.phtml:114
msgid "手机号码"
msgstr ""

#: ../../view/customer/customer/add.phtml:121
#: ../../view/customer/supplier/add.phtml:121
msgid "座机号码"
msgstr ""

#: ../../view/customer/customer/add.phtml:138
#: ../../view/customer/supplier/add.phtml:138
msgid "地区"
msgstr ""

#: ../../view/customer/customer/add.phtml:142
#: ../../view/customer/supplier/add.phtml:142
msgid "修改"
msgstr ""

#: ../../view/customer/customer/add.phtml:145
#: ../../view/customer/customer/add.phtml:231
#: ../../view/customer/supplier/add.phtml:145
#: ../../view/customer/supplier/add.phtml:231
msgid "请选择"
msgstr ""

#: ../../view/customer/customer/add.phtml:166
#: ../../view/customer/supplier/add.phtml:19
#: ../../view/customer/supplier/add.phtml:166
msgid "详细地址"
msgstr ""

#: ../../view/customer/customer/add.phtml:183
#: ../../view/customer/supplier/add.phtml:183
msgid "开户账号"
msgstr ""

#: ../../view/customer/customer/add.phtml:254
#: ../../view/customer/customer/add.phtml:255
msgid "请选择客户分类！"
msgstr ""

#: ../../view/customer/customer/add.phtml:258
msgid "请填写客户名称！"
msgstr ""

#: ../../view/customer/customer/add.phtml:261
msgid "请填写客户编码！"
msgstr ""

#: ../../view/customer/customer/add.phtml:264
#: ../../view/customer/supplier/add.phtml:264
msgid "请选择地区！"
msgstr ""

#: ../../view/customer/customer/index.phtml:87
msgid "您确实要删除该客户吗？"
msgstr ""

#: ../../view/customer/supplier-category/add.phtml:7
#: ../../view/customer/supplier-category/add.phtml:42
msgid "供应商分类编码"
msgstr ""

#: ../../view/customer/supplier-category/add.phtml:11
#: ../../view/customer/supplier-category/add.phtml:50
msgid "供应商分类名称"
msgstr ""

#: ../../view/customer/supplier-category/add.phtml:15
msgid "供应商分类排序"
msgstr ""

#: ../../view/customer/supplier-category/add.phtml:24
#: ../../view/customer/supplier/add.phtml:59
msgid "返回供应商分类列表"
msgstr ""

#: ../../view/customer/supplier-category/add.phtml:25
#: ../../view/customer/supplier-category/add.phtml:70
msgid "保存供应商分类"
msgstr ""

#: ../../view/customer/supplier-category/add.phtml:36
msgid "编辑供应商分类"
msgstr ""

#: ../../view/customer/supplier-category/add.phtml:36
#: ../../view/customer/supplier-category/index.phtml:7
msgid "添加供应商分类"
msgstr ""

#: ../../view/customer/supplier-category/add.phtml:100
msgid "供应商分类编码不能为空！"
msgstr ""

#: ../../view/customer/supplier-category/add.phtml:103
msgid "供应商分类名称不能为空！"
msgstr ""

#: ../../view/customer/supplier-category/index.phtml:47
msgid "您确实要删除该供应商分类吗？"
msgstr ""

#: ../../view/customer/supplier/add.phtml:15
msgid "供应商排序"
msgstr ""

#: ../../view/customer/supplier/add.phtml:60
#: ../../view/customer/supplier/add.phtml:217
msgid "保存供应商"
msgstr ""

#: ../../view/customer/supplier/add.phtml:71
msgid "编辑供应商"
msgstr ""

#: ../../view/customer/supplier/add.phtml:71
#: ../../view/customer/supplier/index.phtml:7
msgid "添加供应商"
msgstr ""

#: ../../view/customer/supplier/add.phtml:254
#: ../../view/customer/supplier/add.phtml:255
msgid "请选择供应商分类！"
msgstr ""

#: ../../view/customer/supplier/add.phtml:258
msgid "请填写供应商名称！"
msgstr ""

#: ../../view/customer/supplier/add.phtml:261
msgid "请填写供应商编码！"
msgstr ""

#: ../../view/customer/supplier/index.phtml:29
msgid "供应商电话 / 手机"
msgstr ""

#: ../../view/customer/supplier/index.phtml:100
msgid "您确实要删除该供应商吗？"
msgstr ""
